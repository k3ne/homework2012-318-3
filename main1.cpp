#include <iostream>
#include <stdlib.h>
#include <vector>
#include <stdio.h>
#include <fstream>
#include <time.h>


using namespace std;

struct node
{
    double delay;
    double AAT;
    double RAT;
    double slack;
    double edge_delay;
    int num;
};

class graph
    {
        private:
        public:
        vector <node> v_n;
        vector <vector <node> > V_out;
        vector <vector <node> > V_in;

        graph() {
        }

        void count_AAT(int node_num) {
            if (v_n[node_num].AAT != -1) {
                return;
            }
            if (V_in[node_num].size() == 0) {
                v_n[node_num].AAT = v_n[node_num].delay;
                return;
            }
            double mas[V_in[node_num].size()];
            double max, tmp;
            for (int i = 0; i < V_in[node_num].size(); i++) {
                if (V_in[node_num][i].AAT == -1) {
                    count_AAT(V_in[node_num][i].num);
                }
                tmp = v_n[node_num].delay + V_in[node_num][i].edge_delay + v_n[V_in[node_num][i].num].AAT;
                if (i == 0) {
                    max = tmp;
                }
                if (tmp > max) {
                    max = tmp;
                }
            }
            v_n[node_num].AAT = max;
        }

        void count_RAT(int node_num) {
            if (v_n[node_num].RAT != -1) {
                return;
            }
            double mas[V_out[node_num].size()];
            double max, tmp;
            for (int i = 0; i < V_out[node_num].size(); i++) {
                if (V_out[node_num][i].RAT == -1) {
                    count_RAT(V_out[node_num][i].num);
                }
                tmp = -v_n[node_num].delay - V_out[node_num][i].edge_delay + v_n[V_out[node_num][i].num].RAT;
                if (i == 0) {
                    max = tmp;
                }
                if (tmp > max) {
                    max = tmp;
                }
            }
            v_n[node_num].RAT = max;
        }
    };

void create_graph(void)
{
    int vertex_count, edge_count;
    cin >> vertex_count >> edge_count;
    int k = vertex_count;
    vector <vector <int> > achievable_vert;
    vector <vector <int> > V_out;
    vector <int> v;

    achievable_vert.push_back(v);
    V_out.push_back(v);


    vertex_count--;
    int used_vertex = 1;
    int tmp_vertex;
    srand(time(NULL));
    int edge_dir;
    while (vertex_count > 0) {
        achievable_vert.push_back(v);
        V_out.push_back(v);
        tmp_vertex = rand() % used_vertex;
        edge_dir = rand() % 2;

        if (edge_dir == 0) {
            V_out[V_out.size() - 1].push_back(tmp_vertex);
            achievable_vert[achievable_vert.size() - 1].push_back(tmp_vertex);
        }

        if (edge_dir == 1) {
            V_out[tmp_vertex].push_back(V_out.size() - 1);
            for(int i = 0; i < achievable_vert.size(); i++) {
                for(int j = 0; j < achievable_vert[i].size(); j++) {
                    if (achievable_vert[i][j] == tmp_vertex) {
                        achievable_vert[i].push_back(achievable_vert.size() - 1);
                    }
                }
            }
        }

        vertex_count--;
        used_vertex++;
        edge_count--;
    }

    vertex_count = k;
    int v1, v2;

    while (edge_count != 0) {
        v1 = rand() % vertex_count;
        v2 = rand() % vertex_count;
        int ind = 0;
        if (v1 == v2) {
            continue;
        }

        for (int i = 0; i < V_out[v1].size(); i++) {
            if (v2 == V_out[v1][i]) {
                ind = 1;
                break;
            }
        }

        for (int i = 0; i < V_out[v2].size(); i++) {
            if (v1 == V_out[v2][i]) {
                ind = 1;
                break;
            }
        }

        if (ind == 1) {
            continue;
        }

        edge_dir = rand() % 2;
        int f = 0, q = 0;

        if (edge_dir == 0) {
            for(int i = 0; i < achievable_vert[v1].size(); i++) {
                if (achievable_vert[v1][i] == v2) {
                    f = 1;
                    break;
                }
            }
            if (f == 0) {
                V_out[v2].push_back(v1);
                for (int i = 0; i < achievable_vert[v2].size(); i++) {
                    if (achievable_vert[v2][i] == v1) {
                        q = 1;
                        break;
                    }
                }
                if (q == 1) {
                    continue;
                }
                for (int i = 0; i < achievable_vert.size(); i++) {
                    for (int j = 0; j < achievable_vert[i].size(); j++) {
                        if (achievable_vert[i][j] == v2) {
                            achievable_vert[i].push_back(v1);
                            break;
                        }
                    }
                }

            } else {
                V_out[v1].push_back(v2);
                for (int i = 0; i < achievable_vert[v1].size(); i++) {
                    if (achievable_vert[v1][i] == v2) {
                        q = 1;
                        break;
                    }
                }
                if (q == 1) {
                    continue;
                }
                for (int i = 0; i < achievable_vert.size(); i++) {
                    for (int j = 0; j < achievable_vert[i].size(); j++) {
                        if (achievable_vert[i][j] == v1) {
                            achievable_vert[i].push_back(v2);
                            break;
                        }
                    }
                }
            }
        }

        if (edge_dir == 1) {
            for(int i = 0; i < achievable_vert[v2].size(); i++) {
                if (achievable_vert[v2][i] == v1) {
                    f = 1;
                    break;
                }
            }
            if (f == 0) {
                V_out[v1].push_back(v2);
                for (int i = 0; i < achievable_vert[v1].size(); i++) {
                    if (achievable_vert[v1][i] == v2) {
                        q = 1;
                        break;
                    }
                }
                if (q == 1) {
                    continue;
                }
                for (int i = 0; i < achievable_vert.size(); i++) {
                    for (int j = 0; j < achievable_vert[i].size(); j++) {
                        if (achievable_vert[i][j] == v1) {
                            achievable_vert[i].push_back(v2);
                            break;
                        }
                    }
                }

            } else {
                V_out[v2].push_back(v1);
                for (int i = 0; i < achievable_vert[v2].size(); i++) {
                    if (achievable_vert[v2][i] == v1) {
                        q = 1;
                        break;
                    }
                }
                if (q == 1) {
                    continue;
                }
                for (int i = 0; i < achievable_vert.size(); i++) {
                    for (int j = 0; j < achievable_vert[i].size(); j++) {
                        if (achievable_vert[i][j] == v2) {
                            achievable_vert[i].push_back(v1);
                            break;
                        }
                    }
                }
            }
        }
        edge_count--;
    }

    ofstream nodes(".nodes.txt", fstream::trunc);
    ofstream nets(".nets.txt", fstream::trunc);
    ofstream aux(".aux.txt", fstream::trunc);

    for (int i = 0; i < V_out.size(); i++) {
        int del = rand() % 100;
        nodes << del << endl;
        if (V_out[i].empty()) {
            int r = rand() % 100;
            aux << i << " " << r << endl;
        }
        for (int j = 0; j < V_out[i].size(); j++) {
            int delay = rand() % 100;
            nets << i << " " << V_out[i][j] << " " << delay << endl;
        }
    }
    nodes.close();
    nets.close();
    aux.close();

    return;
}

int main(void)
{
    create_graph();
    return 0;
    graph G;
    ifstream nodes(".nodes.txt");
    ifstream nets(".nets.txt");
    ifstream aux(".aux.txt");
    double del;
    int node_count = 0;
    struct node tmp_n;
    tmp_n.AAT = -1;
    tmp_n.RAT = -1;
    tmp_n.slack = -1;
    tmp_n.num = -1;
    while (nodes >> del) {
        tmp_n.delay = del;
        tmp_n.num++;
        G.v_n.push_back(tmp_n);
        node_count++;
    }

    vector <node> v_node;
    for (int i = 0; i < node_count; i++) {
        G.V_in.push_back(v_node);
        G.V_out.push_back(v_node);
    }

    int node_num;
    double RAT;
    while (aux >> node_num >> RAT) {
        G.v_n[node_num].RAT = RAT;
    }

    double v_out, v_in, delay;
    while (nets >> v_out >> v_in >> delay) {
        G.v_n[v_out].edge_delay = delay;
        G.v_n[v_in].edge_delay = delay;
        G.V_in[v_in].push_back(G.v_n[v_out]);
        G.V_out[v_out].push_back(G.v_n[v_in]);
    }

    ofstream slack(".slacks.txt", fstream::trunc);
    ofstream result(".result.txt", fstream::trunc);
    int f = 0;

    for (int i = 0; i < node_count; i++) {
        G.count_AAT(i);
        G.count_RAT(i);
        slack << G.v_n[i].RAT - G.v_n[i].AAT << endl;
        G.v_n[i].slack = G.v_n[i].RAT - G.v_n[i].AAT;
        if (G.v_n[i].RAT - G.v_n[i].AAT < 0) {
            f = 1;
        }
    }

     if (f == 0) {
        result << "0";
    } else {
        result << "1" << endl;
        for (int i = 0; i < node_count; i++) {
            if (G.v_n[i].slack < 0) {
                result << i << " ";
            }
        }
    }

    result.close();
    slack.close();
    nets.close();
    nodes.close();
    aux.close();

    return 0;
}
